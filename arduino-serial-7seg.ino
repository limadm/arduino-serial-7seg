/*
 * Serial interface to a 4-digit 7-segment LED display (common anode).
 * 
 * Each digit is a 4-bit BCD sequence sent in order.
 * Data transfer works as a serial-in 16-bit shift register with latched outputs.
 * DAT is read on rising CLK, and the register buffer is latched for display on rising LAT.
 * The input pins have weak pullups, so it is advisable to leave the bus HIGH when unused.
 * 
 * Example, to display "5238", send (0101 0010 0011 1000):
 * 
 *               5               2               3               8
 *         -------------   -------------   -------------   -------------
 *         0   1   0   1   0   0   1   0   0   0   1   1   1   0   0   0
 *             __      __          __              __________             _
 * DAT \______/  \____/  \________/  \____________/          \___________/
 *          _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   ___
 * CLK \___/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/
 * 
 * and then latch it to the output with:
 *        ___
 * LAT \_/
 * 
 * the display should update with the value "5238".
 * The refresh cycle of one digit takes 100µs (~2.5kHz), to make some room for PWM dimming.
 * 
 * author: Daniel Lima (2016)
 * license: MIT
 */

#include <TimerOne.h>

// anodes
#define digit0 2
#define digit1 3
#define digit2 A5
#define digit3 4

// segments
#define segDP 6
#define segA A2
#define segB A1
#define segC 8
#define segD 5
#define segE 7
#define segF A3
#define segG A4

// data lines
#define DAT 10
#define CLK 11
#define LAT 12

const byte segments[] = {
// dp a b c d e f g
  0b10000001,
  0b11001111,
  0b10010010,
  0b10000110,
  0b11001100,
  0b10100100,
  0b10100000,
  0b10001111,
  0b10000000,
  0b10000100,
};
byte digits[] = { 0, 0, 0, 0 };
uint16_t rx = 0;

void setup() {
  pinMode(digit0, OUTPUT);
  pinMode(digit1, OUTPUT);
  pinMode(digit2, OUTPUT);
  pinMode(digit3, OUTPUT);
  pinMode(segDP, OUTPUT);
  pinMode(segA, OUTPUT);
  pinMode(segB, OUTPUT);
  pinMode(segC, OUTPUT);
  pinMode(segD, OUTPUT);
  pinMode(segE, OUTPUT);
  pinMode(segF, OUTPUT);
  pinMode(segG, OUTPUT);
  pinMode(DAT, INPUT_PULLUP);
  pinMode(CLK, INPUT_PULLUP);
  pinMode(LAT, INPUT_PULLUP);
  Timer1.initialize(100);
  Timer1.attachInterrupt(refresh);
  attachInterrupt(CLK, recv,  RISING);
  attachInterrupt(LAT, latch, RISING);
}

void loop() {
  asm volatile ("nop\n\tnop\n\tnop\n\tnop\n");
}

void recv() {
  rx = (rx << 1) | digitalRead(DAT);
}

void latch() {
  digits[0] = segments[(rx >> 12) & 0xF];
  digits[1] = segments[(rx >> 8) & 0xF];
  digits[2] = segments[(rx >> 4) & 0xF];
  digits[3] = segments[rx & 0xF];
}

void refresh() {
  static byte digit = 0;
  // all digits off
  digitalWrite(digit0, LOW);
  digitalWrite(digit1, LOW);
  digitalWrite(digit2, LOW);
  digitalWrite(digit3, LOW);
  // switch segments
  byte s = digits[digit];
  digitalWrite(segDP,bitRead(s,7));
  digitalWrite(segA, bitRead(s,6));
  digitalWrite(segB, bitRead(s,5));
  digitalWrite(segC, bitRead(s,4));
  digitalWrite(segD, bitRead(s,3));
  digitalWrite(segE, bitRead(s,2));
  digitalWrite(segF, bitRead(s,1));
  digitalWrite(segG, bitRead(s,0));
  // switch digit
  switch (digit) {
    case 0: digitalWrite(digit0, HIGH); break;
    case 1: digitalWrite(digit1, HIGH); break;
    case 2: digitalWrite(digit2, HIGH); break;
    case 3: digitalWrite(digit3, HIGH); break;
  }
  // next digit
  digit = (digit+1) % sizeof(digits);
}
