Serial interface to a 4-digit 7-segment LED display (common anode)
------------------------------------------------------------------

Each digit is a 4-bit BCD sequence sent in order.
Data transfer works as a serial-in 16-bit shift register with latched outputs.
DAT is read on rising CLK, and the register buffer is latched for display on rising LAT.
The input pins have weak pullups, so it is advisable to leave the bus HIGH when unused.

Example, to display "5238", send (0101 0010 0011 1000):

	              5               2               3               8
	        -------------   -------------   -------------   -------------
	        0   1   0   1   0   0   1   0   0   0   1   1   1   0   0   0
	            __      __          __              __________             _
	DAT \______/  \____/  \________/  \____________/          \___________/
	         _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   ___
	CLK \___/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/

and then latch it to the output with:

	       ___
	LAT \_/

the display should update with the value "5238".
The refresh cycle of one digit takes 100µs (~2.5kHz), to make some room for PWM dimming.

Author: Daniel Lima (2016)
License: MIT
